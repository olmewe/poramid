﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Poramid {
	public class LevelData {
		public int number;
		public int height;
		public int duration;
		public List<Decoration> decoration;
		public string client;
		public string request;
		public string positive;
		public string negative;

		public LevelData(int number,int height,float sec) {
			this.number = number;
			this.height = height;
			duration = Mathf.RoundToInt(sec*60);
			decoration = new List<Decoration>();
		}
	}

	public struct Decoration {
		public string type;
		public bool isLine;
		public float x,y;

		public Decoration(string type,float x,float y) {
			isLine = false;
			this.type = type;
			this.x = x;
			this.y = y;
		}
		
		public Decoration(string type,float y) {
			isLine = true;
			this.type = type;
			x = 0;
			this.y = y;
		}
	}
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     