﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Poramid {
	public class Menu:MonoBehaviour {
		public static Menu me { get; private set; }
		public Transform tr { get; private set; }

		AudioSource aud;
		Renderer bg;
		Renderer fadeRend;
		Transform selectTr;

		float fade;
		bool closing;
		int select,x,y;
		Vector2 pos;

		void Awake() {
			me = this;
			tr = transform;

			aud = GetComponent<AudioSource>();
			bg = tr.Find("bg").GetComponent<Renderer>();
			fadeRend = tr.Find("fade").GetComponent<Renderer>();
			fadeRend.enabled = true;
			selectTr = tr.Find("select");
			
			fade = 0;
			closing = false;
			if (Level.data != null) {
				select = Level.data.number;
			} else {
				select = 0;
			}
			x = select%5;
			y = select/5;
			UpdatePos();
			selectTr.localPosition = pos;

			for (int a = 0; a < 10; a++) {
				tr.Find(a.ToString()).GetComponent<TextMesh>().text = string.Format("#{0}\n{1}",a+1,Game.me.levels[a].client);
			}
		}
		
		void Update() {
			float t = -.3f*Time.time;
			bg.material.mainTextureOffset = new Vector2(t,t);
			if (closing) {
				if (fade > 0) {
					fade -= Time.deltaTime*1.5f;
					if (fade <= 0) {
						fade = 0;
						aud.Stop();
						UnityEngine.SceneManagement.SceneManager.LoadScene("levelpreview");
						LevelPreview.ending = false;
					} else {
						aud.volume = fade;
					}
					fadeRend.material.color = new Color(0,0,0,1-fade);
				}
			} else {
				if (fade < 1) {
					fade += Time.deltaTime*2;
					if (fade > 1) fade = 1;
					fadeRend.material.color = new Color(0,0,0,1-fade);
				}
				if (In.Press(KeyCode.UpArrow)) {
					aud.PlayOneShot(Game.audScroll);
					y--;
					if (y < 0) y += 2;
					UpdatePos();
				}
				if (In.Press(KeyCode.DownArrow)) {
					aud.PlayOneShot(Game.audScroll);
					y++;
					if (y >= 2) y -= 2;
					UpdatePos();
				}
				if (In.Press(KeyCode.LeftArrow)) {
					aud.PlayOneShot(Game.audScroll);
					x--;
					if (x < 0) x += 5;
					UpdatePos();
				}
				if (In.Press(KeyCode.RightArrow)) {
					aud.PlayOneShot(Game.audScroll);
					x++;
					if (x >= 5) x -= 5;
					UpdatePos();
				}
				if (In.Press(KeyCode.Return)) {
					aud.PlayOneShot(Game.audSelect);
					Level.data = Game.me.levels[select];
					closing = true;
				}
			}
			selectTr.localPosition = Vector2.Lerp(selectTr.localPosition,pos,Time.deltaTime*20);
		}

		void UpdatePos() {
			select = x+y*5;
			pos = new Vector2(x*3.5f-7,-2-y*1.5f);
		}
	}
}