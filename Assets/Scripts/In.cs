﻿using UnityEngine;
using System.Collections;

namespace Poramid {
	public static class In {
		public static bool Press(KeyCode key) {
			return Input.GetKeyDown(key);
		}

		public static bool Hold(KeyCode key) {
			return Input.GetKey(key);
		}

		public static bool Release(KeyCode key) {
			return Input.GetKeyUp(key);
		}
		
		public static float Axis(KeyCode negative,KeyCode positive) {
			return (Input.GetKey(positive) ? 1 : 0) - (Input.GetKey(negative) ? 1 : 0);
		}
	}
}
