﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Poramid {
	public class LevelPreview:MonoBehaviour {
		public static LevelPreview me { get; private set; }
		public Transform tr { get; private set; }
		
		public static RateLevel rateLevel = null;

		AudioSource aud;
		AudioSource audSfx;
		Renderer bg;
		Renderer avatar;
		TextMesh text;
		Renderer pressEnterRend;
		TextMesh pressEnterText;
		Transform pyramidPreviewTr;
		Renderer pyramidPreview;
		Transform timerTr;
		TextMesh timer;
		Renderer fadeRend;
		Transform gradeTr;
		TextMesh grade;
		TextMesh nameText;
		Transform infoTr;
		TextMesh info;

		float character;
		int characterInt;
		int state;
		float fade;
		bool closing;
		int closingTo;
		float opode;

		Vector3 pyramidScale;

		public static bool ending = false;
		bool failed;
		string textString;
		bool played;

		void Awake() {
			me = this;
			tr = transform;

			aud = GetComponent<AudioSource>();
			audSfx = gameObject.AddComponent<AudioSource>();
			bg = tr.Find("bg").GetComponent<Renderer>();
			avatar = tr.Find("avatar").GetComponent<Renderer>();
			text = tr.Find("textbox").GetComponent<TextMesh>();
			text.text = string.Empty;
			pressEnterRend = tr.Find("pressenter").GetComponent<Renderer>();
			pressEnterRend.enabled = false;
			pressEnterText = pressEnterRend.GetComponent<TextMesh>();
			pyramidPreviewTr = tr.Find("preview");
			pyramidPreview = pyramidPreviewTr.GetComponent<Renderer>();
			timerTr = tr.Find("timer");
			timer = timerTr.Find("text").GetComponent<TextMesh>();
			fadeRend = tr.Find("fade").GetComponent<Renderer>();
			fadeRend.enabled = true;
			gradeTr = tr.Find("grade");
			grade = gradeTr.Find("text").GetComponent<TextMesh>();
			nameText = tr.Find("name").GetComponent<TextMesh>();
			infoTr = tr.Find("info");
			info = infoTr.Find("text").GetComponent<TextMesh>();

			character = -40;
			characterInt = 0;
			state = 0;
			fade = 0;
			closing = false;
			closingTo = 0;
			//0: level
			//1: levelpreview+1
			//2: menu
			opode = 1;

			played = false;

			pyramidScale = pyramidPreviewTr.localScale;
			pyramidPreviewTr.localScale = Vector3.zero;
			timerTr.localScale = Vector3.zero;
			gradeTr.localScale = Vector3.zero;
			infoTr.localScale = Vector3.zero;
		}
		
		void Start() {
			if (ending) {
				failed = rateLevel.grade == "F";
				grade.text = rateLevel.grade;
				info.text = string.Format("{0}\n{1}\n{2}\n{3}",
					InfoNumber(rateLevel.areaScore),
				    InfoNumber(rateLevel.shapeScore),
				    InfoNumber(rateLevel.textureScore),
					InfoNumber(rateLevel.total)
				);
				if (!failed) {
					avatar.material.mainTexture = Resources.Load<Texture2D>("sprites/avatar/"+Level.data.number);
					aud.clip = Resources.Load<AudioClip>("audio/failed");
					textString = Level.data.positive;
					if (Level.data.number >= Game.me.levels.Length-1) {
						closingTo = 2;
						pressEnterText.text = "press enter or esc to go to menu";
					} else {
						closingTo = 1;
						pressEnterText.text = "press enter to continue, or esc to go to menu";
					}
					aud.clip = Resources.Load<AudioClip>("audio/succ");
				} else {
					avatar.material.mainTexture = Resources.Load<Texture2D>("sprites/avatar/"+Level.data.number+"p");
					textString = Level.data.negative;
					closingTo = 0;
					pressEnterText.text = "press enter to retry, or esc to go to menu";
					aud.clip = Resources.Load<AudioClip>("audio/fail");
				}
				aud.loop = false;
			} else {
				avatar.material.mainTexture = Resources.Load<Texture2D>("sprites/avatar/"+Level.data.number);
				pyramidPreview.material.mainTexture = Resources.Load<Texture2D>("sprites/pyramids/"+Level.data.number);
				timer.text = string.Format("{0}:{1:D2}",Level.data.duration/60,Level.data.duration%60);
				aud.clip = Resources.Load<AudioClip>("audio/avatarbgm/"+Level.data.number);
				aud.loop = true;
				aud.Play();
				textString = Level.data.request;
				closingTo = 0;
				pressEnterText.text = "press enter to begin, or esc to go to menu";
			}
			nameText.text = Level.data.client;
			audSfx.clip = Resources.Load<AudioClip>("audio/avatarsfx/"+Level.data.number);
		}

		string InfoNumber(float n) {
			if (n < -.5f) return "--";
			if (n >= 1) return "100%";
			if (n <= 0) return "0%";
			return ((int)(n*100))+"%";
		}

		void Update() {
			float t = -.3f*Time.time;
			bg.material.mainTextureOffset = new Vector2(t,t);
			if (closing) {
				if (fade > 0) {
					fade -= Time.deltaTime*1.5f;
					if (fade <= 0) {
						fade = 0;
						aud.Stop();
						if (closingTo == 2) {
							UnityEngine.SceneManagement.SceneManager.LoadScene("menu");
						} else if (closingTo == 1) {
							Level.data = Game.me.levels[Level.data.number+1];
							UnityEngine.SceneManagement.SceneManager.LoadScene("levelpreview");
						} else {
							UnityEngine.SceneManagement.SceneManager.LoadScene("level");
						}
						rateLevel = null;
						ending = false;
					} else {
						aud.volume = fade;
					}
					fadeRend.material.color = new Color(0,0,0,1-fade);
				}
			} else {
				if (fade < 1) {
					fade += Time.deltaTime*2;
					if (fade > 1) fade = 1;
					fadeRend.material.color = new Color(0,0,0,1-fade);
				}
			}
			if (characterInt < textString.Length) {
				if (In.Press(KeyCode.Return) || In.Press(KeyCode.Escape)) {
					character = characterInt = textString.Length;
					opode = 0;
				} else {
					character += Time.deltaTime*40;
					int prevCharacterInt = characterInt;
					characterInt = (int)character;
					while (prevCharacterInt < characterInt) {
						if (textString[prevCharacterInt] != ' ') {
							audSfx.PlayOneShot(audSfx.clip,.5f);
							break;
						}
						prevCharacterInt++;
					}
					if (characterInt > textString.Length) {
						characterInt = textString.Length;
					} else if (characterInt < 0) {
						characterInt = 0;
					}
				}
				if (character < 0) {
					state = -1;
				} else {
					state = (int)(character*3f/textString.Length);
				}
				if (characterInt == textString.Length) {
					text.text = textString;
				}
				text.text = textString.Substring(0,characterInt);
			} else if (!closing) {
				if (opode > 0) {
					opode -= Time.deltaTime*4;
				}
				if (opode <= 0) {
					if (closingTo == 2) {
						if (In.Press(KeyCode.Return) || In.Press(KeyCode.Escape)) {
							audSfx.PlayOneShot(Game.audBack);
							closing = true;
						}
					} else {
						if (In.Press(KeyCode.Return)) {
							audSfx.PlayOneShot(Game.audSelect);
							closing = true;
						} else if (In.Press(KeyCode.Escape)) {
							audSfx.PlayOneShot(Game.audBack);
							closing = true;
							closingTo = 2;
						}
					}
				}
			}
			if (!ending) {
				if (state >= 1) {
					pyramidPreviewTr.localScale = Vector3.Lerp(pyramidPreviewTr.localScale,pyramidScale,Time.deltaTime*8);
					pyramidPreviewTr.localRotation = Quaternion.Euler(-90,Time.time*30,0);
				}
				if (state >= 2) {
					timerTr.localScale = Vector3.Lerp(timerTr.localScale,Vector3.one,Time.deltaTime*8);
					timerTr.localRotation = Quaternion.Euler(0,0,Mathf.Sin(Time.time*1.2f)*5);
				}
			} else {
				if (state >= 0) {
					infoTr.localScale = gradeTr.localScale = Vector3.Lerp(gradeTr.localScale,Vector3.one,Time.deltaTime*8);
					gradeTr.localRotation = Quaternion.Euler(0,0,Mathf.Sin(Time.time*1.2f)*5);
					if (!played) {
						played = true;
						aud.Play();
					}
				}
			}
			pressEnterRend.enabled = characterInt == textString.Length && (Time.time%1) >= .5f;
		}
	}
}