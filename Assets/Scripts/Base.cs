﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Poramid {
	public class Base:MonoBehaviour {
		public Transform tr { get; private set; }

		public bool playerIsClose;
		public bool open;
		public PlayerType playerType;
		public Texture2D texture;

		Transform menu;
		Renderer techTexture;
		TextMesh techName;
		TextMesh techDescription;
		Renderer techArrowLeft;
		Renderer techArrowRight;
		Renderer[] texTexture;
		Renderer texArrowLeft;
		Renderer texArrowRight;
		Transform texSelect;
		Transform select;
		Transform command;

		float openTempo;
		int techSelected;
		bool below;
		int texPage;
		int texSelected;

		const int tools = 4;

		void Awake() {
			tr = transform;
			
			playerIsClose = false;
			open = false;
			playerType = PlayerType.Primitive;
			texture = Brick.textureDef;

			menu = tr.Find("menu");
			techTexture = menu.Find("techtexture").GetComponent<Renderer>();
			techName = menu.Find("techname").GetComponent<TextMesh>();
			techDescription = menu.Find("techdescription").GetComponent<TextMesh>();
			techArrowLeft = menu.Find("techleft").GetComponent<Renderer>();
			techArrowRight = menu.Find("techright").GetComponent<Renderer>();
			texTexture = new Renderer[] {
				menu.Find("tex0").GetComponent<Renderer>(),
				menu.Find("tex1").GetComponent<Renderer>(),
				menu.Find("tex2").GetComponent<Renderer>()
			};
			for (int a = 0; a < texTexture.Length; a++) {
				texTexture[a].material.mainTextureScale = new Vector2(.5f,.5f);
			}
			texArrowLeft = menu.Find("texleft").GetComponent<Renderer>();
			texArrowRight = menu.Find("texright").GetComponent<Renderer>();
			texSelect = menu.Find("texselect");
			select = menu.Find("select");
			command = tr.Find("command");

			openTempo = 0;
			ResizeMenu();
			techSelected = 0;
			below = false;
			texPage = 0;
			texSelected = 0;
			UpdateMenuTech();
			UpdateMenuTex();
			UpdateMenuHighlight();
		}

		void Update() {
			command.localPosition = new Vector3(0,1.3f+Mathf.Sin(Time.time*2)*.05f,-9);
			select.localScale = Vector3.one*(1+(Mathf.Sin(Time.time*5.3f)*.5f+.5f)*.05f);
			if (open) {
				Level.me.SetCamera(-3,3.5f,-1,4.5f);
				if (In.Press(KeyCode.Return)) {
					open = false;
					Level.me.audSfx.PlayOneShot(Game.audBack);
				} else if (!below) {
					if (techSelected > 0 && In.Press(KeyCode.LeftArrow)) {
						techSelected--;
						playerType = (PlayerType)techSelected;
						UpdateMenuTech();
						UpdateMenuHighlight();
						Level.me.UpdatePlayer();
						Level.me.audSfx.PlayOneShot(Game.audScroll);
					}
					if (techSelected < tools-1 && In.Press(KeyCode.RightArrow)) {
						techSelected++;
						playerType = (PlayerType)techSelected;
						UpdateMenuTech();
						UpdateMenuHighlight();
						Level.me.UpdatePlayer();
						Level.me.audSfx.PlayOneShot(Game.audScroll);
					}
					if (In.Press(KeyCode.DownArrow)) {
						below = true;
						UpdateMenuHighlight();
						Level.me.audSfx.PlayOneShot(Game.audScroll);
					}
				} else {
					if (texSelected > 0 && In.Press(KeyCode.LeftArrow)) {
						texSelected--;
						texture = Brick.textures[texSelected];
						UpdateMenuTex();
						UpdateMenuHighlight();
						if (Level.me.brick != null) {
							Level.me.brick.SetBrickTexture(texture);
						}
						Level.me.audSfx.PlayOneShot(Game.audScroll);
					}
					if (texSelected < Brick.textures.Length-1 && In.Press(KeyCode.RightArrow)) {
						texSelected++;
						texture = Brick.textures[texSelected];
						UpdateMenuTex();
						UpdateMenuHighlight();
						if (Level.me.brick != null) {
							Level.me.brick.SetBrickTexture(texture);
						}
						Level.me.audSfx.PlayOneShot(Game.audScroll);
					}
					if (In.Press(KeyCode.UpArrow)) {
						below = false;
						UpdateMenuHighlight();
						Level.me.audSfx.PlayOneShot(Game.audScroll);
					}
				}
				if (openTempo < 1) {
					openTempo += Time.deltaTime*5;
					if (openTempo > 1) openTempo = 1;
					ResizeMenu();
				}
			} else {
				if (playerIsClose && In.Press(KeyCode.Return)) {
					open = true;
					Level.me.audSfx.PlayOneShot(Game.audSelect);
				}
				if (openTempo > 0) {
					openTempo -= Time.deltaTime*5;
					if (openTempo < 0) openTempo = 0;
					ResizeMenu();
				}
			}
		}
		
		void ResizeMenu() {
			menu.localScale = Vector3.one*Tween.EaseOut(openTempo);
			command.localScale = Vector3.one*.5f*Tween.Ease(1-openTempo);
		}

		void UpdateMenuTech() {
			techArrowLeft.enabled = techSelected > 0;
			techArrowRight.enabled = techSelected < tools-1;
			techTexture.material.mainTextureOffset = new Vector2((techSelected%2)*.5f,(techSelected/2)*.5f);
			switch (techSelected) {
				case 0:
					techName.text = "Primitive";
					techDescription.text =
@"Precise, though not efficient,
as bricks tend to be pretty
heavy.";
					break;
				case 1:
					techName.text = "Rope";
					techDescription.text =
@"A bit easier, but less
precise.";
					break;
				case 2:
					techName.text = "Catapult";
					techDescription.text =
@"Quick, but only if you know
how to aim stuff properly.";
					break;
				case 3:
					techName.text = "UFO";
					techDescription.text =
@"MTgyOTcyODk0NzkyMzQ3MjR
icmljazkyMTgzMTg3MmJ1aWx
kcHlyYW1pZDM4NDMy";
					break;
			}
		}

		void UpdateMenuTex() {
			if (texPage > texSelected) {
				texPage = texSelected;
			} else if (texPage < texSelected-2) {
				texPage = texSelected-2;
			}
			texArrowLeft.enabled = texPage > 0;
			texArrowRight.enabled = texPage < Brick.textures.Length-3;
			for (int a = 0; a < 3; a++) {
				texTexture[a].material.mainTexture = Brick.textures[a+texPage];
			}
		}

		void UpdateMenuHighlight() {
			var pos = texTexture[texSelected-texPage].transform.localPosition;
			texSelect.localPosition = new Vector3(pos.x,pos.y,texSelect.localPosition.z);
			if (below) {
				select.localPosition = new Vector3(texSelect.localPosition.x,texSelect.localPosition.y,select.localPosition.z);
			} else {
				var pos2 = techTexture.transform.localPosition;
				select.localPosition = new Vector3(pos2.x,pos2.y,select.localPosition.z);
			}
		}

		void OnTriggerStay2D(Collider2D col) {
			if (col.name != "player") return;
			playerIsClose = true;
			if (Level.me.brick == null) {
				Level.me.SpawnBrick();
				Level.me.audSfx.PlayOneShot(Game.audBase);
			}
		}
		
		void OnTriggerExit2D(Collider2D col) {
			playerIsClose = false;
		}
	}
}