﻿using UnityEngine;
using System.Collections;

namespace Poramid {
	public class SmoothSprite:MonoBehaviour {
		Transform tr,parent;
		float t;
		float from,to,offset;

		void Awake() {
			tr = transform;
		}
		
		void Start() {
			parent = tr.parent;
			from = to = parent.position.y;
			offset = tr.position.y-to;
		}
		
		void Update() {
			t += Time.deltaTime/Time.fixedDeltaTime;
			var pos = Mathf.Lerp(from,to,t);
			tr.position = new Vector3(tr.position.x,pos+offset,tr.position.z);
		}

		void FixedUpdate() {
			t = 0;
			from = to;
			to = parent.position.y;
		}
	}
}