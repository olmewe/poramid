﻿using UnityEngine;
using System.Collections;

namespace Poramid {
	public class PlayerRope:MonoBehaviour {
		Player player;
		Transform spriteTr;
		Renderer sprite;
		Transform ropeTr;
		Renderer rope;
		
		float anim;
		Vector2 ropeEnd;

		static Texture2D[] sprites = null;

		void Awake() {
			player = GetComponent<Player>();
			player.type = PlayerType.Rope;
			spriteTr = player.tr.Find("sprite");
			sprite = spriteTr.GetComponent<Renderer>();
			ropeTr = player.tr.Find("rope");
			rope = ropeTr.GetComponent<Renderer>();
			
			anim = 0;

			if (sprites == null) {
				sprites = new Texture2D[] {
					Resources.Load<Texture2D>("sprites/player/rope0"),
					Resources.Load<Texture2D>("sprites/player/rope1"),
					Resources.Load<Texture2D>("sprites/player/rope2"),
					Resources.Load<Texture2D>("sprites/player/ropefront0"),
					Resources.Load<Texture2D>("sprites/player/ropefront1"),
					Resources.Load<Texture2D>("sprites/player/ropefront2"),
					Resources.Load<Texture2D>("sprites/player/ropeback0"),
					Resources.Load<Texture2D>("sprites/player/ropeback1"),
					Resources.Load<Texture2D>("sprites/player/ropeback2"),
				};
			}
		}

		void Start() {
			ropeEnd = player.tr.localPosition;
			Level.me.SetBrickKinematic(false);
			Level.me.instructions.text = "ARROWS moves, SPACE jumps, Z releases the brick, SHIFT+ARROWS teleports, ESC ends";
		}

		void Update() {
			if (!Level.me.isPlaying || Level.me.@base.open) return;
			if (player.teleportAlphaUpdate) {
				sprite.material.color = new Color(1,1,1,player.teleportAlpha);
				rope.material.color = new Color(1,1,1,player.teleportAlpha);
			}
			if (player.teleporting) return;
			float axis = In.Axis(KeyCode.LeftArrow,KeyCode.RightArrow);
			if (axis != 0) {
				player.Move(axis*PlayerPrimitive.vel);
				anim = (anim+PlayerPrimitive.vel*Time.deltaTime*4)%4;
				if (Mathf.Sign(spriteTr.localScale.x) != Mathf.Sign(axis)) {
					spriteTr.localScale = new Vector3(-spriteTr.localScale.x,spriteTr.localScale.y,spriteTr.localScale.z);
				}
			} else {
				anim = 0;
			}
			if (player.grounded && In.Press(KeyCode.Space)) player.Jump(PlayerPrimitive.jump);
			if (Level.me.brick != null) {
				if (In.Press(KeyCode.Z)) {
					if (Level.me.brick.tr.localPosition.x >= Level.me.positionMin && Level.me.brick.tr.localPosition.x <= Level.me.positionMin+Level.me.positionSize) {
						Level.me.ReleaseBrick();
					} else {
						Level.me.brick.Die();
					}
				}
			}
			Level.me.SetCamera(spriteTr,new Vector2(0,1),3);
		}

		void FixedUpdate() {
			if (Level.me.brick != null) {
				const float maxDist = 2;
				var dir = player.tr.localPosition-Level.me.brick.tr.localPosition;
				float dist = dir.sqrMagnitude;
				if (dist > maxDist*maxDist) {
					dist = Mathf.Sqrt(dist);
					dir = dir/dist;
					Level.me.brick.rb.AddForce(dir*(dist-maxDist)*Level.me.brick.rb.mass,ForceMode2D.Impulse);
				}
			}
		}

		void LateUpdate() {
			int animFrame;
			if (player.grounded) {
				switch ((int)anim) {
					case 1: animFrame = 1; break;
					case 3: animFrame = 2; break;
					default: animFrame = 0; break;
				}
			} else {
				animFrame = 1;
			}
			if (Level.me.brick != null) {
				Level.me.brick.rb.isKinematic = false;
				if ((spriteTr.localScale.x > 0) == (Level.me.brick.tr.localPosition.x > player.tr.localPosition.x)) {
					animFrame += 3;
				} else {
					animFrame += 6;
				}
				ropeEnd = Level.me.brick.tr.localPosition;
			} else {
				ropeEnd = Vector2.Lerp(ropeEnd,new Vector2(spriteTr.position.x-spriteTr.localScale.x*.125f,spriteTr.position.y-.25f),Time.deltaTime*10);
			}
			Vector2 ropeStart = spriteTr.position;
			float dist = Vector2.Distance(ropeStart,ropeEnd);
			ropeTr.position = new Vector3((ropeStart.x+ropeEnd.x)*.5f,(ropeStart.y+ropeEnd.y)*.5f,ropeTr.localPosition.z);
			ropeTr.localRotation = Quaternion.Euler(0,0,Mathf.Atan2(ropeEnd.y-ropeStart.y,ropeEnd.x-ropeStart.x)*Mathf.Rad2Deg);
			ropeTr.localScale = new Vector3(dist,.125f,1);
			rope.material.mainTextureScale = new Vector2(dist*8,1);
			sprite.material.mainTexture = sprites[animFrame];
		}

		public void UpdateBrickPosition() {
			Level.me.brick.tr.localPosition = new Vector3(spriteTr.position.x-1.5f,spriteTr.position.y,Level.me.brick.tr.localPosition.z);
			Level.me.brick.tr.localRotation = Quaternion.identity;
		}
	}
}